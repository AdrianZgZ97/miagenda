﻿using MiAgenda.Models;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using SignalR_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiAgenda.SignalRChat.Hubs
{
    public static class VariablesSignalR
    {
        public static Dictionary<string, Usuarios> usuariosConectados = new Dictionary<string, Usuarios>();
    }
    public class ChatHub : Hub
    {
        private readonly BDCalendarioChatEntities _context;
        public ChatHub()
        {
            _context = new BDCalendarioChatEntities();
        }

        //public async void OnDisconnected()
        //{
        //    VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out Usuarios usuario);
        //    if (VariablesSignalR.usuariosConectados.Any())
        //    {
        //        VariablesSignalR.usuariosConectados.Remove(Context.ConnectionId);
        //    }
        //    await ComunicarAAmigos(usuario);
        //}

        public async Task SendMessage(string user, string message, bool esPrivado, bool esGrupo, string autor, string idGrupo)
        {
            try
            {
                user = user.Trim();
                autor = autor.Trim();
                if (esPrivado)
                {
                    var grupo = _context.Grupos.AsNoTracking().FirstOrDefault(x => x.nombre == "GP_" + user.Trim() + "_" + autor.Trim() || x.nombre == "GP_" + autor.Trim() + "_" + user.Trim());

                    Usuarios usu;
                    VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out usu);
                    var mensaje = new Mensajes()
                    {
                        mensaje = SecurityUtils.Encriptar(message),
                        usuario_id = usu.user_id
                    };
                    mensaje.chat_id = grupo.chat_id;

                    _context.Mensajes.Add(mensaje);
                    _context.SaveChanges();

                    if (VariablesSignalR.usuariosConectados.Any(x => x.Value.usuario == user))
                    {
                        await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.usuario == user).Key)
                        .ReceiveMessage(user, message, esPrivado, autor);
                    }
                    await Clients.Client(Context.ConnectionId).ReceiveMessage(user, message, esPrivado, autor, esGrupo, null);
                }
                else if (esGrupo && idGrupo != string.Empty)
                {
                    var grupo = _context.Grupos.AsNoTracking().FirstOrDefault(x => x.id == idGrupo);
                    var mensaje = new Mensajes()
                    {
                        mensaje = SecurityUtils.Encriptar(message),
                        usuario_id = VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Key == Context.ConnectionId).Value.user_id
                    };
                    mensaje.chat_id = grupo.chat_id;

                    _context.Mensajes.Add(mensaje);
                    _context.SaveChanges();

                    var grupoUsuarios = _context.GruposUsuarios.AsNoTracking().Where(x => x.grupo_id == idGrupo).ToList();
                    var usuariosConectados = VariablesSignalR.usuariosConectados.Values.ToList();
                    foreach (var usu in usuariosConectados)
                    {
                        if (grupoUsuarios.Any(x => x.usuario_id == usu.user_id))
                        {
                            await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.user_id == usu.user_id).Key).ReceiveMessage(user, message, esPrivado, autor, esGrupo, grupo);
                        }
                    }
                }
                else
                {
                    await Clients.All.ReceiveMessage(user, message, esPrivado, autor);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task UserConnected(string name)
        {
            try
            {
                var usuario = _context.Usuarios.FirstOrDefault(x => x.usuario == name);

                if (usuario != null)
                {
                    //Si esta en el diccionario, lo eliminamos porque se ha conectado con otro connectionId
                    if (VariablesSignalR.usuariosConectados.Any(x => x.Value.usuario == usuario.usuario))
                    {
                        var connectionId = VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.usuario == usuario.usuario).Key;
                        VariablesSignalR.usuariosConectados.Remove(connectionId);
                    }
                    VariablesSignalR.usuariosConectados.Add(Context.ConnectionId, usuario);

                    //Comunicamos a los amigos de que nos hemos conectado
                    await ComunicarAAmigos(usuario);

                    await GetMisAmigos();
                }
                else
                {
                    await Clients.Client(Context.ConnectionId).MostrarError("Usuario no registrado en chat");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task UserDisconnected(string name)
        {
            var usuario = VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.usuario == name).Value;

            VariablesSignalR.usuariosConectados.Remove(Context.ConnectionId);

            //Comunicar a los amigos de que nos hemos desconectado
            await ComunicarAAmigos(usuario);
        }

        //public async Task GetUsersConnected()
        //{
        //    try
        //    {
        //        //var usuario = _context.Usuarios.FirstOrDefault(x => x.Nombre == VariablesSignalR.usuariosConectados.FirstOrDefault(a => a.Key == Context.ConnectionId).Value.Nombre);
        //        await Clients.All.SendAsync("UsersConnected", VariablesSignalR.usuariosConectados.Values.ToArray());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public async Task CreateGroup(string nombreUsuario, string nombreGrupo)
        {
            using (var dbTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var chat = _context.Chats.Add(new Chats());
                    _context.SaveChanges();

                    var grupo = _context.Grupos.Add(new Grupos()
                    {
                        id = Guid.NewGuid().ToString(),
                        nombre = nombreGrupo,
                        privado = false,
                        chat_id = chat.id
                    });
                    _context.SaveChanges();

                    var usuario = _context.Usuarios.FirstOrDefault(x => x.usuario == nombreUsuario);
                    _context.GruposUsuarios.Add(new GruposUsuarios()
                    {
                        grupo_id = grupo.id,
                        usuario_id = usuario.user_id,
                        creador = true
                    });
                    _context.SaveChanges();

                    var misGrupos = _context.GruposUsuarios.Where(x => x.Usuarios.usuario == nombreUsuario).Select(x => x.Grupos).ToArray();
                    var misGruposArray = misGrupos.Where(x => !x.privado).ToArray();
                    await Clients.Client(Context.ConnectionId).actualizarGrupos(misGruposArray);

                    dbTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    await Clients.Client(Context.ConnectionId).MostrarError("Error en la BD al crear el grupo.");
                    return;
                }
            }

            
        }

        public async Task AddUserToGroup(string user, string idGrupo)
        {
            try
            {
                var usuario = _context.Usuarios.FirstOrDefault(x => x.usuario.ToUpper() == user.ToUpper());

                if(usuario == null)
                {
                    await Clients.Client(Context.ConnectionId).MostrarError("El usuario introducido no existe.");
                    return;
                }

                if (_context.GruposUsuarios.Any(x => x.usuario_id == usuario.user_id && x.grupo_id == idGrupo))
                {
                    Clients.Client(Context.ConnectionId).SendAsync("MostrarError", "Este usuario ya esta en el grupo.");
                    return;
                }
                var grupo = _context.Grupos.FirstOrDefault(x => x.id == idGrupo);
                _context.GruposUsuarios.Add(new GruposUsuarios()
                {
                    grupo_id = idGrupo,
                    usuario_id = usuario.user_id,
                    creador = false
                });
                _context.SaveChanges();

                //Comprobamos que esta conectado para ordenarle que actualize los grupos
                if (VariablesSignalR.usuariosConectados.Any(x => x.Value.user_id == usuario.user_id))
                {
                    //Ordenamos que actualize los grupos
                    await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.user_id == usuario.user_id).Key).OrdenActualizarGrupos();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task RemoveUserFromGroup(string user, string idGrupo)
        {
            Usuarios usu = null;
            VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out usu);
            if (usu.usuario.ToUpper() == user.ToUpper())
            {
                await Clients.Client(Context.ConnectionId).MostrarError("No te puedes eliminar tu mismo.");
                return;
            }
            try
            {
                var usuarioEliminado = _context.Usuarios.AsNoTracking().FirstOrDefault(x => x.usuario.ToUpper() == user.ToUpper());
                if (usuarioEliminado == null)
                {
                    await Clients.Client(Context.ConnectionId).MostrarError("El nombre introducido no coincide con ningun usuario.");
                }
                else
                {
                    var grupoUsuario = _context.GruposUsuarios.AsNoTracking().FirstOrDefault(x => x.grupo_id == idGrupo && x.usuario_id == usuarioEliminado.user_id);
                    grupoUsuario = _context.GruposUsuarios.Attach(grupoUsuario);
                    _context.GruposUsuarios.Remove(grupoUsuario);
                    _context.SaveChanges();

                    if (VariablesSignalR.usuariosConectados.Any(x => x.Value.usuario == usuarioEliminado.usuario))
                    {
                        //Actualizamos los grupos de la persona eliminada
                        await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.user_id == usuarioEliminado.user_id).Key).OrdenActualizarGrupos();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddFriend(string userInvitado, string userInvitador)
        {
            var invitado = _context.Usuarios.FirstOrDefault(x => x.usuario == userInvitado);
            //var invitado = VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.usuario == userInvitado).Value;
            if (invitado == null)
            {
                await Clients.Client(Context.ConnectionId).MostrarError("El usuario introducido no existe.");
                return;
            }

            VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out Usuarios invitador);
            try
            {
                if (!_context.Amigos.Any(x => (x.usuario1_id == invitado.user_id && x.usuario2_id == invitador.user_id) || (x.usuario2_id == invitado.user_id && x.usuario1_id == invitador.user_id)))
                {
                    _context.Amigos.Add(new Amigos()
                    {
                        usuario1_id = invitador.user_id,
                        usuario2_id = invitado.user_id,
                        fecha = DateTime.Now
                    });
                    _context.SaveChanges();

                    //Actualizamos los amigos del usuario invitado
                    if (VariablesSignalR.usuariosConectados.Any(x => x.Value.usuario == invitado.usuario))
                    {
                        await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.usuario == userInvitado).Key).OrdenActualizarAmigos();
                    }
                    await GetMisAmigos();
                }
                else
                {
                    await Clients.Client(Context.ConnectionId).MostrarError("Ya tienes a " + invitado.usuario + " como amigo.");
                    return;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task GetMisGrupos(string user)
        {
            try
            {
                var misGrupos = _context.GruposUsuarios.Where(x => x.Usuarios.usuario == user).Select(x => x.Grupos).ToArray();
                misGrupos = misGrupos.Where(x => !x.privado).ToArray();
                await Clients.Client(Context.ConnectionId).actualizarGrupos(misGrupos);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public async Task GetMisAmigos()
        {
            try
            {
                //Consultamos nuestro usuario
                VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out Usuarios usuario);

                //Consultamos mis amigos
                var misUsuariosAmigos = _context.Amigos.AsNoTracking().Where(x => x.usuario1_id == usuario.user_id || x.usuario2_id == usuario.user_id).ToList();

                //Cremos un array con los nombres de los usuarios amigos
                List<EstadoAmigo> misAmigos = new List<EstadoAmigo>();
                string nombre;
                foreach (var amigo in misUsuariosAmigos)
                {
                    nombre = _context.Usuarios.Find(amigo.usuario1_id == usuario.user_id ? amigo.usuario2_id : amigo.usuario1_id).usuario;
                    misAmigos.Add(new EstadoAmigo()
                    {
                        Nombre = nombre,
                        Conectado = VariablesSignalR.usuariosConectados.Any(x => x.Value.usuario == nombre)
                    });
                }

                await Clients.Client(Context.ConnectionId).ActualizarAmigos(misAmigos.OrderByDescending(x => x.Conectado).ThenBy(n => n.Nombre));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task GetMensajesGrupo(string idGrupo)
        {
            try
            {
                var grupo = _context.Grupos.AsNoTracking().FirstOrDefault(x => x.id == idGrupo);
                var mensajesChat = _context.Mensajes.Where(x => x.chat_id == grupo.chat_id).ToList();
                mensajesChat.ForEach(x => x.mensaje = SecurityUtils.DesEncriptar(x.mensaje));
                var usuarios = _context.Mensajes.AsNoTracking().Where(x => x.chat_id == grupo.chat_id).Select(x => x.Usuarios).ToList();
                await Clients.Client(Context.ConnectionId).CargarChat(mensajesChat, usuarios);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task GetMensajesPrivado(string user)
        {
            try
            {
                var usuario = _context.Usuarios.AsNoTracking().FirstOrDefault(x => x.usuario == user);
                Usuarios miUsuario = null;
                VariablesSignalR.usuariosConectados.TryGetValue(Context.ConnectionId, out miUsuario);

                Grupos grupoPrivado;

                //Si existe, consultamos los mensajes, y si no, creamos un nuevo grupo privado.
                if (_context.Grupos.Any(x => x.nombre == "GP_" + miUsuario.usuario + "_" + usuario.usuario || x.nombre == "GP_" + usuario.usuario + "_" + miUsuario.usuario))
                {
                    grupoPrivado = _context.Grupos.FirstOrDefault(x => x.nombre == "GP_" + miUsuario.usuario + "_" + usuario.usuario || x.nombre == "GP_" + usuario.usuario + "_" + miUsuario.usuario);
                }
                else
                {
                    var chat = _context.Chats.Add(new Chats());
                    _context.SaveChanges();

                    var grupo = _context.Grupos.Add(new Grupos()
                    {
                        id = Guid.NewGuid().ToString(),
                        nombre = "GP_" + miUsuario.usuario + "_" + usuario.usuario,
                        privado = true,
                        chat_id = chat.id
                    });
                    _context.SaveChanges();

                    //grupo.Usuarios.Add(miUsuario);
                    //grupo.Usuarios.Add(usuario);
                    _context.GruposUsuarios.Add(new GruposUsuarios()
                    {
                        grupo_id = grupo.id,
                        usuario_id = miUsuario.user_id
                    });
                    _context.GruposUsuarios.Add(new GruposUsuarios()
                    {
                        grupo_id = grupo.id,
                        usuario_id = usuario.user_id
                    });
                    _context.SaveChanges();

                    grupoPrivado = grupo;
                }

                var mensajesChat = _context.Mensajes.AsNoTracking().Where(x => x.chat_id == grupoPrivado.chat_id).ToList();
                mensajesChat.ForEach(x => x.mensaje = SecurityUtils.DesEncriptar(x.mensaje));
                var usuarios = _context.Mensajes.AsNoTracking().Where(x => x.chat_id == grupoPrivado.chat_id).Select(x => x.Usuarios).ToList();
                await Clients.Client(Context.ConnectionId).CargarChat(mensajesChat, usuarios);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task GetPersonasGrupo(Grupos grupo)
        {
            //var personas = grupo.Usuarios.ToList();
            var personas = _context.GruposUsuarios.AsNoTracking().Where(x => x.grupo_id == grupo.id).Select(x => x.Usuarios).ToList();
            await Clients.Client(Context.ConnectionId).CargarListaPersonasGrupo(personas);
        }

        //public async Task Register(string email, string userName, string password, string repeatPassword)
        //{
        //    if (email.Trim() != "" && userName.Trim() != "" && password.Trim() != "" && repeatPassword.Trim() != "")
        //    {
        //        if (password == repeatPassword)
        //        {
        //            if (_context.Usuarios.Any(x => x.nombre == userName))
        //            {
        //                await Clients.Client(Context.ConnectionId).MostrarError("Ya existe un usuario con este nombre.");
        //                return;
        //            }
        //            var usuario = new Usuarios()
        //            {
        //                nombre = userName,
        //                contraseña = SecurityUtils.Encriptar(password)
        //            };
        //            _context.Usuarios.Add(usuario);
        //            _context.SaveChanges();

        //            VariablesSignalR.usuariosConectados.Add(Context.ConnectionId, usuario);

        //            //Logeado
        //            await Clients.Client(Context.ConnectionId).SendAsync("Logeado", usuario.nombre, SecurityUtils.DesEncriptar(usuario.contraseña));

        //            //Comunicamos a los amigos de que nos hemos conectado
        //            await ComunicarAAmigos(usuario);

        //            await GetMisAmigos();
        //        }
        //        else
        //        {
        //            await Clients.Client(Context.ConnectionId).SendAsync("MostrarError", "La contraseña no coincide.");
        //        }
        //    }
        //    else
        //    {
        //        await Clients.Client(Context.ConnectionId).SendAsync("MostrarError", "No puede haber campos vacios.");
        //    }
        //}

        private async Task ComunicarAAmigos(Usuarios usuario)
        {
            try
            {
                var amigos = _context.Amigos.Where(x => x.usuario1_id == usuario.user_id || x.usuario2_id == usuario.user_id).ToList();

                foreach (var amigo in amigos)
                {
                    if (amigo.usuario1_id != usuario.user_id && VariablesSignalR.usuariosConectados.Any(x => x.Value.user_id == amigo.usuario1_id))
                    {
                        await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.user_id == amigo.usuario1_id).Key).OrdenActualizarAmigos();
                    }
                    else if (VariablesSignalR.usuariosConectados.Any(x => x.Value.user_id == amigo.usuario2_id))
                    {
                        await Clients.Client(VariablesSignalR.usuariosConectados.FirstOrDefault(x => x.Value.user_id == amigo.usuario2_id).Key).OrdenActualizarAmigos();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
