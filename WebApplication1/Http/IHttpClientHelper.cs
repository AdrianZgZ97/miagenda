﻿using MiAgenda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MiAgenda.Http
{
    public interface IHttpClientHelper<T>
    {
        Task<T> PostLogin(Uri apiUrl, FormUrlEncodedContent content);
        Task<T> GetSingleItemRequest(Uri apiUrl, string token);
        Task<T> GetSingleItemRequest(Uri apiUrl);
        Task<T[]> GetMultipleItemsRequest(Uri apiUrl, string token, FormUrlEncodedContent content);
        Task<T> PostRequest(Uri apiUrl, T postObject, string token);
        Task PutRequest(Uri apiUrl, T putObject, string token);
        Task DeleteRequest(Uri apiUrl, string token);

    }
}