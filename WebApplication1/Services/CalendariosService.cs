﻿using MiAgenda.Http;
using MiAgenda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace MiAgenda.Services
{
    public class CalendariosService
    {
        private readonly string uriLogin = "http://localhost:50378/api/Login/authenticate";
        private readonly string uriEventos = "http://localhost:50378/api/Calendarios/MisCalendarios";

        public List<Calendarios> GetCalendarios(Usuarios usuario)
        {
            var httpClientUsuarios = new HttpClientHelper<LoginResponse>();
            Uri uri = new Uri(uriLogin);
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("Usuario", usuario.usuario),
                new KeyValuePair<string, string>("Contraseña", usuario.password)
            });

            var logeo = httpClientUsuarios.PostLogin(uri, content).Result;

            var httpClientHelper = new HttpClientHelper<Calendarios>();
            content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("user_id", usuario.user_id.ToString()),
                new KeyValuePair<string, string>("usuario", usuario.usuario),
                new KeyValuePair<string, string>("password", usuario.password),
                new KeyValuePair<string, string>("email", usuario.email)
            });
            var calendarios = httpClientHelper.GetMultipleItemsRequest(new Uri(uriEventos), logeo.Token, content).Result;

            return calendarios.ToList();
        }
    }
}