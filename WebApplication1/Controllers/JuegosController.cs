﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class JuegosController : Controller
    {
        // GET: Juegos
        public ActionResult Index()
        {
            return View();
        }

        // GET: Tres en Raya
        public ActionResult TresEnRaya()
        {
            return View("TresEnRaya");
        }
    }
}