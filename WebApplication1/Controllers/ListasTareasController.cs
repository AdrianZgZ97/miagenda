﻿using MiAgenda.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class ListasTareasController : Controller
    {
        private BDCalendarioChatEntities db = new BDCalendarioChatEntities();

        // GET: ListasTareas
        public ActionResult Index()
        {
            var usuario = Session["userMiAgenda"] as Usuarios;
            var listas = db.ListasTareasUsuariosSet.Where(x => x.user_id == usuario.user_id).Select(x => x.ListasTareas).ToList();
            return View(listas);
        }

        // GET: ListasTareas/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ListasTareas listasTareas = db.ListasTareas.Find(id);
        //    if (listasTareas == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(listasTareas);
        //}

        // GET: ListasTareas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ListasTareas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "lista_id,nombre")] ListasTareas listasTareas)
        {

            if (ModelState.IsValid)
            {
                //Comprobamos si el usuario tiene una lista con ese mismo nombre
                var usuario = Session["userMiAgenda"] as Usuarios;
                var listasTareasUsuario = db.ListasTareasUsuariosSet.Where(x => x.user_id == usuario.user_id).Select(x => x.ListasTareas).ToList();

                var cal = listasTareasUsuario.Where(x => x.nombre == listasTareas.nombre.ToUpper()).ToList();
                if (cal.Count() > 0)
                {
                    ViewBag.error = "Ya tienes una lista con ese nombre.";
                    return View();
                }

                listasTareas.ListasTareasUsuariosSet.Add(new ListasTareasUsuariosSet()
                {
                    lista_id = listasTareas.lista_id,
                    user_id = usuario.user_id,
                    creador = true
                });
                db.ListasTareas.Add(listasTareas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(listasTareas);
        }

        // GET: ListasTareas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ListasTareas listasTareas = db.ListasTareas.Find(id);
            if (listasTareas == null)
            {
                return HttpNotFound();
            }
            return View(listasTareas);
        }

        // POST: ListasTareas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "lista_id,nombre")] ListasTareas listasTareas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listasTareas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(listasTareas);
        }

        // GET: ListasTareas/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ListasTareas listasTareas = db.ListasTareas.Find(id);
        //    if (listasTareas == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(listasTareas);
        //}

        // POST: ListasTareas/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ListasTareas listasTareas = db.ListasTareas.Find(id);

        //    db.ListasTareas.Remove(listasTareas);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        // POST: ListaTareas/DeleteList/5
        [HttpPost]
        public ActionResult DeleteList(int id)
        {
            var usuario = Session["userMiAgenda"] as Usuarios;
            ListasTareas lista = db.ListasTareas.Find(id);
            ListasTareasUsuariosSet listUsu = db.ListasTareasUsuariosSet.Find(id, usuario.user_id);

            if (listUsu.creador)
            {
                db.ListasTareas.Remove(lista);
                db.SaveChanges();
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult AddUserToList(int lista_id, string nombre)
        {
            //if (!Regex.IsMatch(emailUsuarioNuevo, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            var usuarioInvitado = db.Usuarios.FirstOrDefault(x => x.usuario == nombre);
            if (usuarioInvitado == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var existeCalendario = db.ListasTareasUsuariosSet.Where(x => x.lista_id == lista_id && x.user_id == usuarioInvitado.user_id).FirstOrDefault();
            if (existeCalendario != null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Ambiguous);
            }

            var listaTareas = db.ListasTareas.Find(lista_id);
            db.ListasTareasUsuariosSet.Add(new ListasTareasUsuariosSet()
            {
                creador = false,
                user_id = usuarioInvitado.user_id,
                lista_id = listaTareas.lista_id
            });
            db.SaveChanges();

            var usuarioInvitador = Session["userMiAgenda"] as Usuarios;
            EmailUtility.MandarCorreo(usuarioInvitado, usuarioInvitador, listaTareas);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
