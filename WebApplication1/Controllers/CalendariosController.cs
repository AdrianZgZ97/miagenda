﻿using MiAgenda.Filters;
using MiAgenda.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    [AutorizadoFilter]
    public class CalendariosController : Controller
    {
        /*delete from Tareas;
        delete from ListasTareas;
        delete from ListasTareasUsuariosSet;
        delete from Eventos;
        delete from CalendariosUsuariosSet;
        delete from Calendarios;
        delete from Usuarios;
        DBCC CHECKIDENT('Usuarios', RESEED, 0);*/

        private readonly BDCalendarioChatEntities db = new BDCalendarioChatEntities();
        //private CalendariosService calendariosService;

        public CalendariosController()
        {
            //calendariosService = new CalendariosService();
        }

        // GET: Calendarios
        public ActionResult Index()
        {
            var usuario = Session["userMiAgenda"] as Usuarios;

            // Consultando a webapi
            //var calendarios = calendariosService.GetCalendarios(usuario);

            var calendarios = db.CalendariosUsuariosSet.Where(x => x.user_id == usuario.user_id).Select(x => x.Calendarios).ToList();
            return View(calendarios);
        }

        // GET: Calendarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calendarios calendarios = db.Calendarios.Find(id);
            if (calendarios == null)
            {
                return HttpNotFound();
            }
            return View(calendarios);
        }

        // GET: Calendarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Calendarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Calendarios calendarios)
        {
            //Comprobamos si el usuario tiene un calendario con ese mismo nombre
            var usuario = Session["userMiAgenda"] as Usuarios;
            var calendariosUsuario = db.CalendariosUsuariosSet.Where(x => x.user_id == usuario.user_id).Select(x => x.Calendarios).ToList();

            var cal = calendariosUsuario.Where(x => x.nombre == calendarios.nombre.ToUpper()).ToList();
            if (cal.Count() > 0)
            {
                ViewBag.error = "Ya tienes un calendario con ese nombre.";
                return View();
            }

            if (ModelState.IsValid)
            {
                //db.Usuarios.Attach(usuario);
                //calendarios.Usuarios.Add(usuario);

                calendarios.CalendariosUsuariosSet.Add(new CalendariosUsuariosSet()
                {
                    calendario_id = calendarios.calendario_id,
                    user_id = usuario.user_id,
                    creador = true
                });
                calendarios.nombre = calendarios.nombre.ToUpper();

                db.Calendarios.Add(calendarios);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(calendarios);
        }

        // GET: Calendarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calendarios calendarios = db.Calendarios.Find(id);
            if (calendarios == null)
            {
                return HttpNotFound();
            }
            return View(calendarios);
        }

        // POST: Calendarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "calendario_id,nombre")] Calendarios calendarios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calendarios).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calendarios);
        }

        // GET: Calendarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calendarios calendarios = db.Calendarios.Find(id);
            if (calendarios == null)
            {
                return HttpNotFound();
            }
            return View(calendarios);
        }

        // POST: Calendarios/DeleteCalendar/5
        [HttpPost]
        public ActionResult DeleteCalendar(int id)
        {
            var usuario = Session["userMiAgenda"] as Usuarios;
            Calendarios calendarios = db.Calendarios.Find(id);
            CalendariosUsuariosSet calUsu = db.CalendariosUsuariosSet.Find(id, usuario.user_id);

            if (calUsu.creador)
            {
                db.Calendarios.Remove(calendarios);
                db.SaveChanges();
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        // POST: Calendarios/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Calendarios calendarios = db.Calendarios.Find(id);
            db.Calendarios.Remove(calendarios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddUserToCalendar(int calendario_id, string emailUsuarioNuevo)
        {
            if (!Regex.IsMatch(emailUsuarioNuevo, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var usuarioInvitado = db.Usuarios.Where(x => x.email == emailUsuarioNuevo).FirstOrDefault();
            if (usuarioInvitado == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var existeCalendario = db.CalendariosUsuariosSet.Where(x => x.calendario_id == calendario_id && x.user_id == usuarioInvitado.user_id).FirstOrDefault();
            if (existeCalendario != null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Ambiguous);
            }

            var calendario = db.Calendarios.Find(calendario_id);
            db.CalendariosUsuariosSet.Add(new CalendariosUsuariosSet()
            {
                creador = false,
                user_id = usuarioInvitado.user_id,
                calendario_id = calendario.calendario_id
            });
            db.SaveChanges();

            var usuarioInvitador = Session["userMiAgenda"] as Usuarios;
            EmailUtility.MandarCorreo(usuarioInvitado, usuarioInvitador, calendario);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult RemoveUserFromCalendar(int calendario_id, int user_id)
        {
            //falta comprobar  que el que elimina es el creador, por si acaso
            var calendario = db.Calendarios.Find(calendario_id);
            if(calendario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "El calendario no existe.");
            }
            var calendarioUsuario = calendario.CalendariosUsuariosSet.FirstOrDefault(x => x.user_id == user_id);
            if (calendarioUsuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "El usuario no existe.");
            }
            calendario.CalendariosUsuariosSet.Remove(calendarioUsuario);
            db.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
