﻿using MiAgenda.Models;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class ChatController : Controller
    {
        /*delete from Amigos;
        delete from Mensajes;
        delete from Chats;
        delete from GruposUsuarios;
        delete from Grupos;
        delete from Usuarios;
        DBCC CHECKIDENT('Usuarios', RESEED, 0);*/

        private readonly BDCalendarioChatEntities db = new BDCalendarioChatEntities();

        // GET: Chat
        public ActionResult Index()
        {
            var usuario = Session["userMiAgenda"] as Usuarios;
            var usuarioChat = db.Usuarios.FirstOrDefault(x => x.user_id == usuario.user_id);

            ViewBag.username = usuarioChat.usuario;

            return View();
        }
    }
}