﻿using MiAgenda.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class TareasController : Controller
    {
        private BDCalendarioChatEntities db = new BDCalendarioChatEntities();

        // GET: Tareas
        public ActionResult Index(int lista_id)
        {
            //Cuando accedemos poniendo la url a mano
            //if(Session["lista"] == null && lista_id == 0)
            //{
            //    return new HttpUnauthorizedResult();
            //}
            //
            //else if(lista_id == 0)
            //{
            //    lista_id = (Session["lista"] as ListasTareas).lista_id;
            //}
            var tareas = db.Tareas.Include(t => t.ListasTareas).Where(x => x.lista_id == lista_id).ToList();

            Session["lista"] = db.ListasTareas.Find(lista_id);

            return View(tareas);
        }

        // GET: Tareas/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Tareas tareas = db.Tareas.Find(id);
        //    if (tareas == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tareas);
        //}

        // GET: Tareas/Create
        public ActionResult Create()
        {
            ViewBag.lista_id = new SelectList(db.ListasTareas, "lista_id", "nombre");
            return View();
        }

        // POST: Tareas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tarea_id,titulo,estado,lista_id")] Tareas tareas)
        {
            if (ModelState.IsValid)
            {
                tareas.lista_id = (Session["lista"] as ListasTareas).lista_id;
                db.Tareas.Add(tareas);
                db.SaveChanges();
                return RedirectToAction("Index", new { tareas.lista_id });
            }

            //ViewBag.lista_id = new SelectList(db.ListasTareas, "lista_id", "nombre", tareas.lista_id);
            return View(tareas);
        }

        // GET: Tareas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tareas tareas = db.Tareas.Find(id);
            if (tareas == null)
            {
                return HttpNotFound();
            }
            ViewBag.lista_id = new SelectList(db.ListasTareas, "lista_id", "nombre", tareas.lista_id);
            return View(tareas);
        }

        // POST: Tareas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tarea_id,titulo,estado,lista_id")] Tareas tareas)
        {
            if (ModelState.IsValid)
            {
                tareas.lista_id = (Session["lista"] as ListasTareas).lista_id;
                db.Entry(tareas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", tareas.lista_id);
            }
            ViewBag.lista_id = new SelectList(db.ListasTareas, "lista_id", "nombre", tareas.lista_id);
            return View(tareas);
        }

        // GET: Tareas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tareas tareas = db.Tareas.Find(id);
            if (tareas == null)
            {
                return HttpNotFound();
            }
            return View(tareas);
        }

        // POST: Tareas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tareas tareas = db.Tareas.Find(id);
            db.Tareas.Remove(tareas);
            db.SaveChanges();
            return RedirectToAction("Index", tareas.lista_id);
        }

        // POST: Tareas/Done/5
        [HttpGet]
        public ActionResult Done(int id)
        {
            try
            {
                var tarea = db.Tareas.Find(id);
                tarea.estado = !tarea.estado;
                db.Entry(tarea).State = EntityState.Modified;
                db.SaveChanges();
                int lista_id = tarea.ListasTareas.lista_id;
                return RedirectToAction("Index", new { lista_id = lista_id });
                //return new HttpStatusCodeResult(200);
            }
            catch
            {
                return new HttpStatusCodeResult(500);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
