﻿using MiAgenda.Filters;
using MiAgenda.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    [AutorizadoFilter]
    public class EventsController : Controller
    {
        private BDCalendarioChatEntities db = new BDCalendarioChatEntities();
        // GET: Events
        public JsonResult GetEventos()
        {
            var calendario = Session["calendar"] as Calendarios;
            var eventos = db.Eventos.Where(x => x.calendario_id == calendario.calendario_id).ToList();
            var lista = new List<object>();
            eventos.ForEach(x => lista.Add(new
            {
                evento_id = x.evento_id,
                titulo = x.titulo,
                descripcion = x.descripcion,
                empieza = x.empieza,
                termina = x.termina,
                tipo = x.tipo,
                fechaEvento = x.fechaEvento.ToString(),
                calendario_id = x.calendario_id,
                estado = x.estado
            }));

            var data = new JsonResult { Data = lista, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            return data;
        }

        // GET: Events/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            //var opcsTodoElDia = new List<SelectListItem>() {
            //    new SelectListItem(){ Text = "Si", Value = "1" },
            //    new SelectListItem(){ Text = "No", Value = "0" }
            //};
            var tipo = new List<SelectListItem>() {
                new SelectListItem(){ Text = "", Value = "0" },
                new SelectListItem(){ Text = "Normal", Value = "1" },
                //new SelectListItem(){ Text = "No", Value = "0" }
            };
            //ViewBag.TodoElDia = opcsTodoElDia;
            ViewBag.Tipo = tipo;
            ViewData["fechaActual"] = DateTime.Now.ToString();
            return View();
        }

        // POST: Events/Create
        [HttpPost]
        public ActionResult Create(Eventos evento)
        {
            evento.todoElDia = true;
            evento.estado = EstadosEvento.Pendiente.Description();
            if (ModelState.IsValid)
            {
                evento.Calendarios = db.Calendarios.Find();
                db.Eventos.Add(evento);
                db.SaveChanges();
                return RedirectToAction("Home", "Users");
            }
            return View(evento);
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Events/Edit
        [HttpPost]
        public ActionResult EditarEvento(Eventos evento)
        {
            try
            {
                evento.calendario_id = (Session["calendar"] as Calendarios).calendario_id;
                if (evento.evento_id == 0)
                {
                    evento.fechaEvento = new DateTime(evento.fechaEvento.Value.Year, evento.fechaEvento.Value.Month, evento.fechaEvento.Value.Day, evento.fechaEvento.Value.Hour, evento.fechaEvento.Value.Minute, 0);
                    evento.estado = EstadosEvento.Pendiente.Description();
                    db.Eventos.Add(evento);
                    db.SaveChanges();
                    return new HttpStatusCodeResult(200);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var eve = db.Eventos.Find(evento.evento_id);
                        eve.descripcion = evento.descripcion;
                        eve.titulo = evento.titulo;
                        eve.empieza = evento.empieza;
                        eve.termina = evento.termina;
                        eve.todoElDia = evento.todoElDia;
                        eve.fechaEvento = evento.fechaEvento;
                        eve.tipo = evento.tipo;
                        db.Entry(eve).State = EntityState.Modified;
                        //db.Eventos.Attach(evento);
                        db.SaveChanges();
                        return new HttpStatusCodeResult(200);
                    }
                    return new HttpStatusCodeResult(401);
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500);
            }
        }

        //// POST: Events/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Events/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Events/Delete/5
        [HttpPost]
        public ActionResult DeleteSecundary(int id)
        {
            try
            {
                var evento = db.Eventos.Find(id);
                db.Eventos.Remove(evento);
                db.SaveChanges();
                return new HttpStatusCodeResult(200);
            }
            catch
            {
                return new HttpStatusCodeResult(500);
            }
        }

        // POST: Events/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: Events/Done/5
        [HttpPost]
        public ActionResult Done(int id)
        {
            try
            {
                var evento = db.Eventos.Find(id);
                evento.estado = EstadosEvento.Terminado.Description();
                db.Entry(evento).State = EntityState.Modified;
                db.SaveChanges();
                return new HttpStatusCodeResult(200);
            }
            catch
            {
                return new HttpStatusCodeResult(500);
            }
        }
    }
}
