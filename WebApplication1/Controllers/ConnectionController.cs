﻿using MiAgenda.Models;
using MiAgenda.SignalRChat.Hubs;
using System.Linq;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class ConnectionController : Controller
    {
        private BDCalendarioChatEntities _context = new BDCalendarioChatEntities();

        [HttpGet]
        public ActionResult Prueba()
        {
            return new HttpStatusCodeResult(200);
        }
        [HttpPost]
        public ActionResult Connect(string name, string password)
        {
            if (_context.Usuarios.Any(x => x.usuario == name && SecurityUtils.DesEncriptar(x.password) == password))
            {
                if (!VariablesSignalR.usuariosConectados.ContainsKey(name))
                {
                    return new HttpStatusCodeResult(200);
                }
                else
                {
                    return new HttpStatusCodeResult(401);
                }
            }
            else if (!_context.Usuarios.Any(x => x.usuario == name))
            {
                return new HttpStatusCodeResult(200);
            }
            return new HttpStatusCodeResult(401);
        }
    }
}