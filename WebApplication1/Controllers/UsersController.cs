﻿using MiAgenda.Filters;
using MiAgenda.Models;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace MiAgenda.Controllers
{
    public class UsersController : Controller
    {
        private readonly BDCalendarioChatEntities db = new BDCalendarioChatEntities();

        // GET: Users/Login
        [AllowAnonymous]
        public ActionResult Login()
        {

            return View();
        }

        // GET: Users/Home
        [AutorizadoFilter]
        public ActionResult Home(int calendario)
        {
            var cal = db.Calendarios.Find(calendario);
            Session["calendar"] = cal;
            ViewBag.calendario = cal.nombre;
            return View();
        }

        //POST: Users/Login
        [HttpPost]
        [AllowAnonymous]
        [ActionName("Login")]
        public ActionResult Login(Usuarios user)
        {
            var usuario = db.Usuarios.Where(u => u.usuario == user.usuario).FirstOrDefault();
            if (usuario == null)
            {
                ViewBag.error = "El usuario introducido no existe.";
                return View();
            }

            var password = user.password.Encriptar();
            if (usuario.password != password)
            {
                ViewBag.error = "La contraseña es incorrecta.";
                return View();
            }
            Session["userMiAgenda"] = usuario;

            return RedirectToAction("Index", "Calendarios");
        }

        // GET: Users/Logout
        public ActionResult Logout()
        {
            Session["userMiAgenda"] = null;
            Session["calendar"] = null;
            Session["lista"] = null;

            return View("Login");
        }

        // GET: Users
        //public ActionResult Index()
        //{
        //    //Si esta logeado, ya no se puede volver a logear
        //    if (ComprobarLogeado())
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        //    }
        //    return View(db.Usuarios.ToList());
        //}

        // GET: Users/Details/5
        //public ActionResult Details(int? id)
        //{
        //    //Si esta logeado, ya no se puede volver a logear
        //    if (ComprobarLogeado())
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        //    }

        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Usuarios users = db.Usuarios.Find(id);
        //    if (users == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(users);
        //}

        // GET: Users/Create
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Usuarios users)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (db.Usuarios.Any(x => x.usuario == users.usuario))
                    {
                        ViewBag.descripcion = "Ese nombre de usuario ya existe.";
                        return View(users);
                    }
                    if (!Regex.IsMatch(users.email, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
                    {
                        ViewBag.error = "El email no es valido.";
                        return View(users);
                    }
                    users.password = users.password.Encriptar();
                    var user = db.Usuarios.Add(users);
                    db.SaveChanges();

                    //Session["userMiAgenda"] = dbCalendario.Usuarios.OrderByDescending(x => x.user_id).FirstOrDefault();
                    Session["userMiAgenda"] = user;
                    return RedirectToAction("Index", "Calendarios");
                }
            }
            catch (Exception ex)
            {
                ViewBag.titulo = (int)HttpStatusCode.InternalServerError + " " + HttpStatusCode.InternalServerError;
                ViewBag.descripcion = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return View("~/Views/Shared/Error.cshtml");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    //Si esta logeado, ya no se puede volver a logear
        //    if (ComprobarLogeado())
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        //    }

        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Usuarios users = db.Usuarios.Find(id);
        //    if (users == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(users);
        //}

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "user_id,usuario,password")] Usuarios users)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(users).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(users);
        //}

        // GET: Users/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    //Si esta logeado, ya no se puede volver a logear
        //    if (ComprobarLogeado())
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        //    }

        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Usuarios users = db.Usuarios.Find(id);
        //    if (users == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(users);
        //}

        // POST: Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Usuarios users = db.Usuarios.Find(id);
        //    db.Usuarios.Remove(users);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
