﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_core.Models
{
    public class EstadoAmigo
    {
        public string Nombre { get; set; }
        public bool Conectado { get; set; }
    }
}
