﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MiAgenda.Models
{
    public enum EstadosEvento
    {
        [Description("Pendiente")]
        Pendiente = 0,
        [Description("Terminado")]
        Terminado = 1
    }
}