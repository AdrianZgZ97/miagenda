//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MiAgenda.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GruposUsuarios
    {
        public string grupo_id { get; set; }
        public int usuario_id { get; set; }
        public bool creador { get; set; }
    
        public virtual Grupos Grupos { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}
