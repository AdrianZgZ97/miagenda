﻿using System;
using System.Text;

namespace MiAgenda.Models
{
    public class EmailUtility
    {
        public static void MandarCorreo<T>(Usuarios usuarioInvitado, Usuarios usuarioInvitador, T objeto)
        {
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                mail.To.Add(usuarioInvitado.email);
                mail.Subject = "El usuario " + usuarioInvitador.usuario + " te ha añadido a una agenda.";
                mail.SubjectEncoding = Encoding.UTF8;

                if (objeto is Calendarios)
                {
                    var calendario = objeto as Calendarios;
                    mail.Body = "<h3>Ahora puedes acceder a la agenda " + calendario.nombre + ".</h3>";
                }
                else if (objeto is ListasTareas)
                {
                    var listaTareas = objeto as ListasTareas;
                    mail.Body = "<h3>Ahora puedes acceder a la lista de tareas " + listaTareas.nombre + ".</h3>";
                }

                //mail.Body = "<h3>Ahora puedes acceder a la agenda " + (typeof(Calendarios) == objeto.GetType() ? (objeto as Calendarios).nombre : (objeto as ListasTareas).nombre) + ".</h3>";
                mail.BodyEncoding = Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.From = new System.Net.Mail.MailAddress("aplicacionmiagenda@gmail.com");
                //Contraseña del correo aplicacionmiagenda@gmail.com: fThEyVd05

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Credentials = new System.Net.NetworkCredential("aplicacionmiagenda@gmail.com", "fThEyVd05");//correo y contraseña

                client.Port = 587;
                client.EnableSsl = true;

                client.Host = "smtp.gmail.com";// mail.dominio.com


                client.Send(mail);
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}