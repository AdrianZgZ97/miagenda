﻿using MiAgenda.Models;
using System.Web;
using System.Web.Mvc;

namespace MiAgenda.Filters
{
    public class AutorizadoFilter : AuthorizeAttribute
    {
        protected virtual Usuarios CurrentUser
        {
            get { return System.Web.HttpContext.Current.Session["userMiAgenda"] as Usuarios; }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = System.Web.HttpContext.Current.Session["userMiAgenda"] as Usuarios;
            return user != null;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RedirectToRouteResult routeData = null;

            if (CurrentUser == null)
            {
                routeData = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (new
                    {
                        controller = "Users",
                        action = "Login",
                    }
                    ));
            }
            else
            {
                routeData = new RedirectToRouteResult
                (new System.Web.Routing.RouteValueDictionary
                 (new
                 {
                     controller = "Error",
                     action = "AccessDenied"
                 }
                 ));
            }

            filterContext.Result = routeData;
        }
    }
}