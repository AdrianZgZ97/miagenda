﻿//Mostramos solo loader
$("#chatPage").hide();
$(".loader").show();

//Gestionamos loader
var myVar;
myVar = setTimeout(showPage, 3000);

function showPage() {
    $(".loader").hide();
    $("#chatPage").show();
}

var name = "";
var misGrupos;
var modoPrivado = false;
var modoGrupo = false;
var privado = "";
var grupo = "";
var estadoAmigo = false;

//Botones de grupo
$("#btnVerPersonasGrupo").hide();
$("#listaGrupos").hide();
$("#listaAmigos").hide();

var connection = $.connection.chatHub;

connection.client.receiveMessage = function (user, message, esPrivado, autor, esGrupo, objectGrupo) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = "";
    var li = document.createElement("li");
    //li.className = "list-group-item";
    if (esGrupo && grupo.nombre != objectGrupo.nombre) {
        var grupos = $("#grupos div div");
        for (var g of grupos) {
            if (g.innerHTML.indexOf(objectGrupo.nombre) != -1 && g.childNodes.length == 1) {
                g.innerHTML += ' <span class="badge badge-info">' + 1 + '</span>';
            } else if (g.innerHTML.indexOf(objectGrupo.nombre) != -1) {
                var pendientes = +g.childNodes[1].innerHTML;
                g.childNodes[1].innerHTML = pendientes + 1;
            }
        }
        return;
    } else if (esGrupo && grupo.nombre == objectGrupo.nombre) {
        //alert("Grupo");
        if (autor == name) {
            //alert("Autor mensaje");
            encodedMsg = "Yo: " + msg;
            li.className += " text-right rounded";
        } else {
            //alert("No es autor de mensaje");
            encodedMsg = user + ": " + msg;
            li.className += " text-left rounded";
        }
    } else if (esPrivado && privado == "") {
        var usuarios = $("#amigos div div");
        for (var u of usuarios) {
            if (u.innerHTML.indexOf(autor) != -1 && u.childNodes.length == 1) {
                u.innerHTML += ' <span class="badge badge-secondary">' + 1 + '</span>';
            } else if (u.innerHTML.indexOf(autor) != -1) {
                var pendientes = +u.childNodes[1].innerHTML;
                u.childNodes[1].innerHTML = pendientes + 1;
            }
        }
    } else if (esPrivado) {
        if (autor == name) {
            //alert("Autor mensaje");
            encodedMsg = "Yo: " + msg;
            li.className += " text-right rounded";
        } else {
            //alert("No es autor de mensaje");
            encodedMsg = autor + ": " + msg;
            li.className += " text-left rounded";
        }
    } else {
        //alert("General o privado");
        //Cuando lo unico que es diferente de los 3 es user que es la persona receptora
        if (autor == name && user != name) {
            encodedMsg = "Yo: " + msg;
            li.className += " text-right rounded";
        } else if (user != name) {
            if (document.getElementById("tipoMensaje").innerHTML == "<b>**Hablando en la sala general**</b>") {
                encodedMsg = user + ": " + msg;
                li.className += " text-left rounded";
            }
        } else {
            encodedMsg = "Yo: " + msg;
            li.className += " text-right rounded";
        }
    }
    li.innerHTML = encodedMsg;
    //li.style = "border: none";
    document.getElementById("messagesList").appendChild(li);
    var elmnt = document.getElementById("content");
    elmnt.scrollIntoView(false);
};
connection.client.actualizarAmigos = function (misAmigos) {
    document.getElementById("amigos").innerHTML = "";
    if (misAmigos.length > 0) {
        for (var u of misAmigos) {
            var divCol = document.createElement("div");
            divCol.className = "col-3";
            var divCard = document.createElement("div");
            divCard.href = "#";
            //if (u.conectado == true) {
            divCard.onclick = function (e) {
                if (modoGrupo) {
                    modoGrupo = false;
                    grupo = "";
                    $("#btnInvitar").hide();
                    $("#btnEliminar").hide();
                    $("#btnVerPersonasGrupo").hide();

                    $("#grupos div div").each(function (index, value) {
                        if ($("#grupos div div")[index].className == "list-group-item list-group-item-info active") {
                            $("#grupos div div")[index].className = "list-group-item list-group-item-info";
                        }
                    });
                } else if (modoPrivado) {
                    $("#amigos div div").each(function (index, value) {
                        if ($("#amigos div div")[index].className == "list-group-item list-group-item-info active") {
                            $("#amigos div div")[index].className = "list-group-item list-group-item-info";
                        }
                    });
                }
                if (privado == e.target.innerHTML) {
                    //Informamos que esta hablando en la sala general
                    document.getElementById("tipoMensaje").innerHTML = "<b>**Hablando en la sala general**</b>";
                    document.getElementById("messagesList").innerHTML = "";

                    modoPrivado = false;
                    privado = "";

                    if (e.target.className == "list-group-item list-group-item-info active") {
                        e.target.className = "list-group-item list-group-item-info";
                    } else {
                        if (estadoAmigo == true) {
                            e.target.className = "list-group-item list-group-item-info";
                        } else {
                            e.target.className = "list-group-item list-group-item-light active";
                        }
                    }
                } else {
                    //Informamos que esta hablando en privado
                    document.getElementById("tipoMensaje").innerHTML = "<b>**Hablando en privado con " + e.target.innerHTML + "**</b>";
                    document.getElementById("messagesList").innerHTML = "";

                    modoPrivado = true;

                    if (e.target.childNodes.length > 1) {
                        e.target.childNodes[1].remove();
                    }
                    privado = e.target.innerHTML;

                    //Controlamos el estilo de la card dependiendo si esta conectado o no
                    if (e.target.className == "list-group-item list-group-item-info") {
                        estadoAmigo = true;
                        e.target.className = "list-group-item list-group-item-info active";
                    } else {
                        if (estadoAmigo == true) {
                            e.target.className = "list-group-item list-group-item-info";
                        } else {
                            e.target.className = "list-group-item list-group-item-light active";
                        }
                    }

                    //Cargamos el chat del grupo
                    connection.invoke("GetMensajesPrivado", privado).catch(function (err) {
                        alert(err);
                        return;
                    });
                }
            };
            //}
            divCard.innerHTML = u.Nombre;

            divCard.className = "list-group-item list-group-item-info";
            if (u.Conectado == false) {
                divCard.className = "list-group-item list-group-item-light";
            }

            divCol.appendChild(divCard);
            document.getElementById("amigos").appendChild(divCol);
        }
    } else {
        var span = document.createElement("span");
        span.innerHTML = "No hay amigos conectados.";
        document.getElementById("amigos").appendChild(span);
    }
};
connection.client.actualizarGrupos = function (misGrupos) {
    document.getElementById("grupos").innerHTML = "";
    //console.log(misGrupos);
    //misGrupos = JSON.parse(misGrupos);
    if (misGrupos.length > 0) {
        for (var g of misGrupos) {
            var divCol = document.createElement("div");
            divCol.className = "col-3";
            var divCard = document.createElement("div");

            divCard.href = "#";
            divCard.onclick = function (e) {
                if (modoPrivado) {
                    modoPrivado = false;
                    privado = "";
                    $("#amigos div div").each(function (index, value) {
                        if ($("#amigos div div")[index].className == "list-group-item list-group-item-action active") {
                            $("#amigos div div")[index].className = "list-group-item list-group-item-action";
                        }
                    });
                }
                for (var elem of misGrupos) {
                    //console.log(elem);
                    if (e.target.innerHTML.indexOf(elem.nombre) != -1 && e.target.className == "list-group-item list-group-item-action") {
                        //Si modoGrupo es true es que ya hay otro grupo seleccionado
                        if (modoGrupo) {
                            $("#grupos div div").each(function (index, value) {
                                if ($("#grupos div div")[index].className == "list-group-item list-group-item-action active") {
                                    $("#grupos div div")[index].className = "list-group-item list-group-item-action";
                                }
                            });
                        }
                        grupo = elem;

                        modoGrupo = true;
                        e.target.className = "list-group-item list-group-item-action active";

                        if (e.target.childNodes.length > 1) {
                            e.target.childNodes[1].remove();
                        }

                        $("#btnVerPersonasGrupo").show();
                        //Informamos que esta hablando en la sala general
                        document.getElementById("tipoMensaje").innerHTML = "<b>** Hablando por el grupo \"" + e.target.innerHTML + "\" **</b>";

                        //Cargamos el chat del grupo
                        connection.invoke("GetMensajesGrupo", grupo.id).catch(function (err) {
                            alert(err);
                            return;
                        });
                        return;
                    } else if (e.target.innerHTML.indexOf(elem.nombre) != -1 && e.target.className == "list-group-item list-group-item-action active") {
                        grupo = "";

                        modoGrupo = false;
                        e.target.className = "list-group-item list-group-item-action";
                        $("#btnVerPersonasGrupo").hide();

                        //Informamos que esta hablando en la sala general
                        document.getElementById("tipoMensaje").innerHTML = "<b>** Hablando en la sala general **</b>";
                        document.getElementById("messagesList").innerHTML = "";
                    }
                }
            };
            divCard.innerHTML = g.nombre;
            divCard.className = "list-group-item list-group-item-action";
            divCol.appendChild(divCard);
            document.getElementById("grupos").appendChild(divCol);
        };
    } else {
        var span = document.createElement("span");
        span.innerHTML = "No tienes grupos.";
        document.getElementById("grupos").appendChild(span);
    }
};
connection.client.ordenActualizarGrupos = function () {
    solicitarGrupos();
};
connection.client.ordenActualizarAmigos = function () {
    solicitarAmigos();
};
connection.client.cargarChat = function (mensajes, usuarios) {
    document.getElementById("messagesList").innerHTML = "";
    for (var m in mensajes) {
        //console.log(m);
        //console.log(mensajes[m])
        var li = document.createElement("li");
        //li.className = "list-group-item";
        if (usuarios[m].usuario == name) {
            //alert("Autor mensaje");
            li.innerHTML = "Yo: " + mensajes[m].mensaje;
            li.className += " text-right";
        } else {
            //alert("No es autor de mensaje");
            li.innerHTML = usuarios[m].usuario + ": " + mensajes[m].mensaje;
            li.className += " text-left";
        }
        document.getElementById("messagesList").appendChild(li);
    }
    var elmnt = document.getElementById("content");
    elmnt.scrollIntoView(false);
};
connection.client.mostrarError = function (error) {
    alert(error);
};
connection.client.cargarListaPersonasGrupo = function (personas) {
    var ul = document.getElementById("listaPersonasGrupo");
    ul.innerHTML = "";
    for (let p of personas) {
        var li = document.createElement("li");
        li.innerHTML = p.usuario;
        ul.appendChild(li);
    }
    if (personas.length <= 1) {
        $("#btnEliminar").hide();
    } else {
        $("#btnEliminar").show();
    }
    $('#modalPersonasGrupo').modal();
};

$.connection.hub.start({ waitForPageLoad: false }).done(function () {

    //Informamos que esta hablando en la sala general
    document.getElementById("tipoMensaje").innerHTML = "<b>**Hablando en la sala general**</b>";

    if (sessionStorage.getItem("ChatSignalR") != null) {
        name = sessionStorage.getItem("ChatSignalR");
    } else {
        sessionStorage.setItem("ChatSignalR", $("#usuario").val());
        name = $("#usuario").val();
    }
    connection.server.userConnected(name.toString()).fail(function (err) {
        alert(err.toString());
    });
    solicitarGrupos();
});
window.addEventListener("beforeunload", function (e) {
    connection.invoke("UserDisconnected", name).catch(function (err) {
        alert(err);
        return;
    });
});

//Eventos onclick
document.getElementById("createGroupButton").onclick = function () {
    $('#modalCrearGrupo').modal();
};
document.getElementById("btnInvitar").onclick = function (e) {
    document.getElementById("tituloInvitarAGrupo").innerHTML = "Invitando al grupo " + grupo.nombre;
    document.getElementById("btnInvitarAGrupo").innerHTML = "Invitar";
    $('#modalPersonasGrupo').modal('hide');
    $('#modalInvitarAGrupo').modal();
};
document.getElementById("addFriendButton").onclick = function () {
    document.getElementById("tituloAgregarAmigo").innerHTML = "Agregando amigo";
    $('#modalAgregarAmigo').modal();
};
document.getElementById("btnEliminar").onclick = function () {
    $('#modalPersonasGrupo').modal('hide');
    //Utilizamos la misma modal que el de invitar persona
    document.getElementById("tituloInvitarAGrupo").innerHTML = "Eliminando del grupo";
    document.getElementById("btnInvitarAGrupo").innerHTML = "Eliminar";
    $('#modalInvitarAGrupo').modal();
};
document.getElementById("btnVerPersonasGrupo").onclick = function () {
    connection.invoke("GetPersonasGrupo", grupo).catch(function (err) {
        alert(err);
        return;
    });
};
document.getElementById("btnGrupos").onclick = function () {
    $("#listaGrupos").slideToggle();
};
document.getElementById("btnAmigos").onclick = function () {
    $("#listaAmigos").slideToggle();
};

//Hasta que nos conectemos, ponemos que no hay usuarios conectados
var span = document.createElement("span");
span.innerHTML = "No hay amigos conectados.";
document.getElementById("amigos").appendChild(span);

//Hasta que nos conectemos, ponemos que no grupos disponibles
var span = document.createElement("span");
span.innerHTML = "No tienes grupos.";
document.getElementById("grupos").appendChild(span);

//Ajustar el chat a la dimension de la pantalla
var height = $(window).height();
var width = $(window).width();

$('#chat').height(height);
$('#chat').width(width);

function sendMessage() {
    //var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    if (message.trim() == "") {
        event.preventDefault();
        return;
    }
    document.getElementById("messageInput").value = "";
    connection.invoke("SendMessage", modoPrivado ? privado : name, message, modoPrivado, modoGrupo, name, grupo ? grupo.id : "").catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
}
function connect() {
    var userName = document.getElementById("userNameLogin").value;
    var pass = document.getElementById("userPasswordLogin").value;
    if (userName.trim() != "" && pass.trim() != "") {
        connection.invoke("UserConnected", userName, pass).catch(function (err) {
            alert(err.toString());
        });
    }
    //event.preventDefault();
}
function solicitarGrupos() {
    connection.invoke("GetMisGrupos", name).catch(function (err) {
        alert(err);
        return;
    });
}
function solicitarAmigos() {
    connection.invoke("GetMisAmigos").catch(function (err) {
        alert(err);
        return;
    });
}
function register() {
    var email = document.getElementById("userEmail").value;
    var userName = document.getElementById("userName").value;
    var password = document.getElementById("userPassword").value;
    var repeatPassword = document.getElementById("userRepeatPassword").value;

    if (email.trim() != "" &&
        userName.trim() != "" &&
        password.trim() != "" &&
        repeatPassword.trim() != "") {
        if (password == repeatPassword) {
            $("#txtErrorRegister").hide();
            connection.invoke("Register", email, userName, password, repeatPassword).catch(function (err) {
                alert(err.toString());
            });
        } else {
            $("#txtErrorRegister").text("La contraseña no coincide.");
            $("#txtErrorRegister").show();
        }
    } else {
        $("#txtErrorRegister").text("No puede haber campos vacios.");
        $("#txtErrorRegister").show();
    }
}
function agregarAmigo() {
    var user = document.getElementById("txtNombreAmigo").value;
    if (user != "") {
        connection.invoke("AddFriend", user, name).catch(function (err) {
            alert(err);
            return;
        });
    }
    $('#modalAgregarAmigo').modal('hide');
}
function invitarAGrupo() {
    var user = document.getElementById("txtNombreUsuario").value;
    if (grupo != undefined && user != "") {
        if (document.getElementById("btnInvitarAGrupo").innerHTML == "Invitar") {
            //alert("Invitar");
            connection.invoke("AddUserToGroup", user, grupo.id).catch(function (err) {
                alert(err);
                return;
            });
        } else {
            connection.invoke("RemoveUserFromGroup", user, grupo.id).catch(function (err) {
                alert(err);
                return;
            });
        }
    }
    $('#modalInvitarAGrupo').modal('hide');
}
function crearGrupo() {
    if (document.getElementById("txtNombreGrupo").value != "") {
        connection.server.createGroup(name, document.getElementById("txtNombreGrupo").value).fail(function (err) {
            alert(err);
        });
        //connection.invoke("CreateGroup", name, document.getElementById("txtNombreGrupo").value).catch(function (err) {
        //    alert(err);
        //    return;
        //});
        document.getElementById("txtNombreGrupo").value = "";
        $('#modalCrearGrupo').modal('hide');
    }
}