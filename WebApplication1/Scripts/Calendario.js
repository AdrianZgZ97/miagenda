﻿$(document).ready(function () {
    //Codigo para saber si se esta ejecutando desde un dispositivo movil
    var uAg = navigator.userAgent.toLowerCase();
    var isMobile = !!uAg.match(/android|iphone|ipad|ipod|blackberry|symbianos/i);

    var events = [];
    //Evento seleccionado por defecto
    var selectedEvent = {
        eventoId: 0,
        Tipo: 1,
        title: "",
        description: "",
        start: Fechas(true)
    };
    //Metodo de ayuda con las fechas
    function Fechas(fechaActual = true, otraFecha) {
        if (fechaActual) {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            //alert("fechas " + dd + " " + mm + " " + yyyy)
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return dd + '/' + mm + '/' + yyyy;
        } else {
            var fecha = new Date(otraFecha);

            var dd = fecha.getDate();
            var mm = fecha.getMonth() + 1; //January is 0!

            var yyyy = fecha.getFullYear();

            //alert("fechas " + dd + " " + mm + " " + yyyy)
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            //Aqui lo hago al reves porque el formato es mm/dd/yyyy
            return dd + '/' + mm + '/' + yyyy;
        }
    }
    //Metodo de ayuda con las horas
    function Horas(horaActual = true, otraHora) {
        if (horaActual) {
            var today = new Date();
            var hh = today.getHours();
            var mm = today.getMinutes();

            return hh + ':' + mm;
        } else {
            var fecha = new Date(otraHora);
            //alert(fecha)
            var hh = fecha.getHours();
            var mm = fecha.getMinutes();

            var minutos = '';
            if (mm < 10) {
                minutos = '0' + mm;
            } else {
                minutos = mm;
            }
            //alert("horas " + hh + ":" + minutos)
            return hh + ':' + minutos;
        }
    }
    ActualizarYGenerarCalendario();
    //Consulta con ajax para tener los eventos y pasarlos al calendario
    function ActualizarYGenerarCalendario() {
        events = [];
        //moment.locale('es');
        $.ajax({
            type: "GET",
            url: "/events/GetEventos",
            success: function (data) {
                //alert(moment.locale())
                //console.log(data);
                $.each(data, function (i, v) {
                    if (v.tipo == 1) {
                        var fecha = v.fechaEvento.split(' ')[0];
                        var hora = v.fechaEvento.split(' ')[1];
                        var fechaEvento = new Date(fecha.split('/')[2], fecha.split('/')[1] - 1, fecha.split('/')[0], hora.split(':')[0], hora.split(':')[1]);
                        //alert(fechaEvento);
                        events.push({
                            title: v.titulo,
                            description: v.descripcion,
                            start: fechaEvento,
                            end: v.termina != null ? fechaEvento : null,
                            color: v.estado == 'Terminado' ? '#0c0' : '#c00',
                            eventoId: v.evento_id,
                            tipo: v.tipo,
                            estado: v.estado
                        });
                    }
                })

                GenerateCalender(events);
            },
            error: function (error) {
                alert('failed');
            }
        })
    }
    //Genera el calendario con todos los datos
    function GenerateCalender(events) {
        //console.log(events);
        $('#calender').fullCalendar('destroy');
        $('#calender').fullCalendar({
            contentHeight: 400,
            defaultDate: new Date(),
            timeFormat: 'h(:mm)a',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            eventLimit: true,
            eventColor: '#00ff00',
            events: events,
            locale: "es",
            eventClick: function (calEvent, jsEvent, view) {
                selectedEvent = calEvent;
                //alert(calEvent.start)
                //selectedEvent.start = Fechas(false, calEvent.start.format("DD/MM/YYYY")) + ' ' + Horas(false, calEvent.start)
                $('#myModal #eventTitle').text(calEvent.title);
                var $contenido = $('<div/>');
                if (calEvent.tipo != undefined && calEvent.tipo == 1) {
                    $contenido.append($('<p/>').html('<b>Fecha: </b>' + calEvent.start.format("DD/MM/YYYY") + ' ' + Horas(false, calEvent.start)));
                } else {
                    //$contenido.append($('<p/>').html('<b>Empieza: </b>' + calEvent.start));
                    //if (calEvent.end != null) {
                    //    $contenido.append($('<p/>').html('<b>Termina: </b>' + calEvent.end.format("DD/MM/YYYY HH:mm a")));
                    //}
                }
                if (calEvent.description != null) {
                    $contenido.append($('<p/>').html('<b>Descripción: </b>' + calEvent.description));
                }
                $contenido.append($('<p/>').html('<b>Estado: </b>' + calEvent.estado));
                if (selectedEvent.estado.trim() == 'Terminado') {
                    $('#btnHecho').hide();
                    $('#btnEditar').hide();
                } else {
                    $('#btnHecho').show();
                    $('#btnEditar').show();
                }

                $('#myModal #pDetails').empty().html($contenido);
                $('#myModal').modal();
            },
            selectable: true,
            select: function (start, end) {
                if (new Date(start) > new Date() ||
                    (new Date(start).getDate() == new Date().getDate() &&
                        new Date(start).getMonth() == new Date().getMonth() &&
                        new Date(start).getFullYear() == new Date().getFullYear())) {
                    //alert("select " + Fechas(false, start))
                    selectedEvent = {
                        eventoId: 0,
                        Tipo: 1,
                        title: "",
                        description: "",
                        start: Fechas(false, start)
                    };
                    //console.log(selectedEvent)
                    FormularioEditar();
                    $('#calender').fullCalendar('unselect');
                } else {
                    return;
                }

            }
        })
    }
    //Evento del boton crear
    $('#btnCrear').click(function () {
        selectedEvent = {
            eventoId: 0,
            Tipo: 1,
            title: "",
            description: "",
            start: Fechas(true)
        };
        FormularioEditar(false, true);
    })
    //Evento del boton editar
    $('#btnEditar').click(function () {
        FormularioEditar(true, true);
    })
    //Evento del boton eliminar
    $('#btnEliminar').click(function () {
        if (selectedEvent != null && confirm("¿Estas seguro/a?")) {
            $.ajax({
                type: "POST",
                url: "/events/DeleteSecundary",
                data: { 'id': selectedEvent.eventoId },
                success: function (data) {
                    ActualizarYGenerarCalendario();
                    $('#myModal').modal('hide');
                },
                error: function (error) {
                    alert('failed to delete');
                }
            })
        }
    })
    //Evento del boton eliminar
    $('#btnHecho').click(function () {
        if (selectedEvent != null && confirm("¿Estas seguro/a?")) {
            $.ajax({
                type: "POST",
                url: "/events/Done",
                data: { 'id': selectedEvent.eventoId },
                success: function (data) {
                    ActualizarYGenerarCalendario();
                    $('#myModal').modal('hide');
                },
                error: function (error) {
                    alert('failed to done');
                }
            })
        }
    })
    //Fecha evento
    $('#dp1').datepicker({
        format: "dd/mm/yyyy",
        minDate: new Date(),
        autoclose: true,
        todayHighlight: true
    });
    //Hora evento
    $('#tp1').datetimepicker({
        format: 'HH:mm'
    });
    //Rellenando el formulario en modal para editar un evento
    function FormularioEditar(editar = false, habilitarFecha = false) {
        if (habilitarFecha) {
            $('#txtFechaEvento').prop('disabled', false);
        }
        if (selectedEvent != null) {
            $('#tituloEditar').text(editar == true ? 'Editando evento' : 'Creando evento');
            $('#EventoID').val(selectedEvent.eventoId);
            $('#comboTipo').val(selectedEvent.tipo);
            $('#txtTitulo').val(selectedEvent.title);
            $('#txtDescripcion').val(selectedEvent.description);
            //alert(selectedEvent.start)
            if (editar) {
                $('#txtFechaEvento').val(Fechas(false, selectedEvent.start));
                var horaSinFormatear = new Date(selectedEvent.start);
                var horaCompleta = horaSinFormatear.toTimeString().split(' ')[0];
                var trozosHora = horaCompleta.split(':');
                var hora = trozosHora[0] + ":" + trozosHora[1];
                $('#txtHoraEvento').val(hora);
            } else {
                $('#txtFechaEvento').val(selectedEvent.start);
                var horaCompleta = new Date().toTimeString().split(' ')[0];
                var trozosHora = horaCompleta.split(':');
                var hora = trozosHora[0] + ":" + trozosHora[1];
                $('#txtHoraEvento').val(hora);
            }

        }
        $('#myModal').modal('hide');
        $('#myModalEdit').modal();
    }
    //Evento del boton guardar tanto para crear como para editar
    $('#btnGuardar').click(function () {
        //Validacion
        if ($('#comboTipo').val() == null) {
            alert("El tipo de evento es obligatorio.");
            return;
        }
        if ($('#txtTitulo').val().trim() == '') {
            alert("El titulo es obligatorio.");
            return;
        }

        if ($('#txtFechaEvento').val().trim() == '') {
            alert("La fecha del evento es obligatoria.");
            return;
        }
        if ($('#txtHoraEvento').val().trim() == '') {
            alert("La hora del evento es obligatoria.");
            return;
        }
        var fechaTrozeada = $('#txtFechaEvento').val().split('/');
        var horaTrozeada = $('#txtHoraEvento').val().split(':');
        var fechaRecibida = new Date(fechaTrozeada[2], fechaTrozeada[1] - 1, fechaTrozeada[0], horaTrozeada[0], horaTrozeada[1]);
        //alert(fechaRecibida + ' ' + new Date())
        if (fechaRecibida < new Date()) {
            alert("La fecha del evento no puede ser mas antigua que la fecha actual.");
            return;
        }

        var fechaEvento = new Date(fechaTrozeada[2], fechaTrozeada[1] - 1, fechaTrozeada[0], horaTrozeada[0], horaTrozeada[1]);
        //var calendario = $('#Calendario').val();
        var data = {
            evento_id: $('#EventoID').val(),
            tipo: $('#comboTipo').val().trim(),
            titulo: $('#txtTitulo').val().trim(),
            descripcion: $('#txtDescripcion').val().trim(),
            fechaEvento: fechaEvento.toLocaleString(),
            calendario_id: 1
        }
        //console.log(data);
        //Accion
        GuardarEvento(data);
    });
    //Post con ajax para el controlador
    function GuardarEvento(data) {
        $.ajax({
            type: "POST",
            url: "/Events/EditarEvento",
            data: { 'evento': data },
            success: function (data) {
                ActualizarYGenerarCalendario();
                $('#myModalEdit').modal('hide');
            },
            error: function (error) {
                alert('failed to edit');
            }
        })
    }
})