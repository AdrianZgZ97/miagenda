﻿var turnoRojo = true;
var pelotas = new Array(33);

function comprobarDiagonales(pelota, fila) {
    //Contador para las pelotas del mismo color consecutivas.
    let contador = 0;
    //Guardamos el color de la actual pelota
    let color = pelotas[pelota];
    //Obtenemos el tablero para recorrer las columnas e ir pasando por la misma fila.
    let tablero = document.getElementById("tablero");

    let f = fila;
    //Primera pelota de la diagonal superior izquierda
    let pelota1;
    //Primera pelota de la diagonal inferior izquierda
    let pelota2;

    //Buscamos la primera pelota de la diagonal superior izquierda
    for (let i = +(pelota.toString()[0]); i > 0; i--) {
        let columna = tablero.childNodes[i];
        pelota1 = +(columna.id.toString() + f);
        f--;
        if (f < 1) {
            break;
        }
    }
    //alert(pelota1);

    f = fila;
    //Buscamos la primera pelota de la diagonal inferior izquierda
    for (let i = +(pelota.toString()[0]); i > 0; i--) {
        let columna = tablero.childNodes[i];
        pelota2 = +(columna.id.toString() + f);
        f++;
        if (f > 3) {
            break;
        }
    }
    //alert(pelota2);
    f = +(pelota1.toString()[1]);
    //Recorremos la diagonal derecha abajo.
    for (let i = +(pelota1.toString()[0]); i < tablero.childNodes.length; i++) {
        let columna = tablero.childNodes[i];
        //Construimos el id del siguiente en la fila
        let p = +(columna.id.toString()[0] + f);
        //alert(p);
        //Comprobamos que tenga color
        if (pelotas[p] != undefined) {
            //Si tiene color y coincide, sumamos al contador, si no, reiniciamos el contador
            if (pelotas[p] == color) {
                contador++;
            } else {
                contador = 0;
            }
            //Si el contador llega a 4, es que hemos encontrado 4 pelotas seguidad del mismo color.
            if (contador == 3) {
                return true;
            }
        }
        f++;
        if (f > 3) {
            break; s
        }
    }

    contador = 0;
    f = +(pelota2.toString()[1]);
    //Recorremos la diagonal derecha arriba.
    for (let i = +(pelota2.toString()[0]); i < tablero.childNodes.length; i++) {
        let columna = tablero.childNodes[i];
        //Construimos el id del siguiente en la fila
        let p = +(columna.id.toString()[0] + f);
        //alert(p);
        //Comprobamos que tenga color
        if (pelotas[p] != undefined) {
            //Si tiene color y coincide, sumamos al contador, si no, reiniciamos el contador
            if (pelotas[p] == color) {
                contador++;
            } else {
                contador = 0;
            }
            //Si el contador llega a 4, es que hemos encontrado 4 pelotas seguidad del mismo color.
            if (contador == 3) {
                return true;
            }
        }
        f--;
        if (f < 1) {
            break;
        }
    }
    return false;
}

function comprobarVertical(pelota, columna) {
    let contador = 0;
    //Guardamos el color de la actual pelota
    let color = pelotas[pelota];
    //Recorremos las 3 pelotas de debajo de la actual para saber si son del mismo color.
    for (let i = pelota; i < pelota + 3; i++) {
        if (pelotas[i] == color) {
            contador++;
        } else {
            break;
        }
    }
    if (contador == 3) {
        return true;
    }

    return false;
}

function comprobarHorizontal(pelota, fila) {
    //Contador para las pelotas del mismo color consecutivas.
    let contador = 0;
    //Guardamos el color de la actual pelota
    let color = pelotas[pelota];
    //Obtenemos el tablero para recorrer las columnas e ir pasando por la misma fila.
    let tablero = document.getElementById("tablero");
    //Recorremos columnas
    for (let i = 1; i < tablero.childNodes.length; i++) {
        let columna = tablero.childNodes[i];
        //Construimos el id del siguiente en la fila
        let p = +(columna.id.toString()[0] + fila);
        //Comprobamos que tenga color
        if (pelotas[p] != undefined) {
            //Si tiene color y coincide, sumamos al contador, si no, reiniciamos el contador
            if (pelotas[p] == color) {
                contador++;
            } else {
                contador = 0;
            }
            //Si el contador llega a 4, es que hemos encontrado 4 pelotas seguidad del mismo color.
            if (contador == 3) {
                return true;
            }
        }
    }
    return false;
}

function comprobarColumnaLlena(columna) {
    let contador = 0;
    let idColumna = columna.id;

    for (let i = idColumna * 10; i < idColumna * 10 + 3; i++) {
        if (pelotas[i] != undefined) {
            contador++;
        }
    }
    if (contador == 3) {
        return true;
    } else {
        return false;
    }
}

function finJuego() {
    let tablero = document.getElementById("tablero");
    //Recorremos columnas
    let columnas = tablero.childNodes;
    for (let c of columnas) {
        for (let p of c.childNodes) {
            p.onclick = null;
        }
    }
    let h = document.getElementById("fin");
    h.innerHTML = "Fin del juego";
    let t = document.getElementById("turno");
    t.innerHTML = "";
}

function click(e) {
    var columna;
    var fila;
    var id;
    id = e.target.id;
    columna = document.getElementById(id).parentNode;

    if (!comprobarColumnaLlena(columna)) {
        if (pelotas[e.target.id] == undefined) {
            if (turnoRojo) {
                pelotas[e.target.id] = "rojo";
                columna.childNodes[+(e.target.id.toString()[1]) - 1].style.background = "red";
            }
            else {
                pelotas[e.target.id] = "azul";
                columna.childNodes[+(e.target.id.toString()[1]) - 1].style.background = "blue";
            }
        } else {
            return;
        }
        fila = e.target.id.toString();
        fila = fila[1];
        if (comprobarVertical(e.target.id, columna) || comprobarHorizontal(e.target.id, fila) || comprobarDiagonales(e.target.id, fila)) {
            if (turnoRojo)
                alert("Ha ganado el jugador Rojo");
            else
                alert("Ha ganado el jugador Azul");
            finJuego();
            return;
        }
        turnoRojo = !turnoRojo;
        let t = document.getElementById("turno");
        t.innerHTML = turnoRojo ? "Turno del rojo" : "Turno del azul";
        t.style.color = turnoRojo ? "red" : "blue";
    } else {
        alert("Esta columna esta completa.");
    }
}

function crearTablero() {
    let tablero = document.getElementById("tablero");
    //Columnas
    for (var i = 0; i < 3; i++) {
        var columna = document.createElement("div");
        columna.id = i + 1;
        //Filas
        for (var x = 0; x < 3; x++) {
            var bola = document.createElement("div");
            bola.id = (10 * (i + 1)) + x + 1;
            bola.className = "pelota";
            bola.onclick = click;
            columna.appendChild(bola);
        }
        tablero.appendChild(columna);
    }
    let t = document.getElementById("turno");
    t.innerHTML = turnoRojo ? "Turno del rojo" : "Turno del azul";
    t.style.color = turnoRojo ? "red" : "blue";
}

window.onload = function () {
    crearTablero();
}

