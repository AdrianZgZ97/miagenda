﻿$(document).ready(function () {
    $("#lblTitulo").hide();
    $("#Titulo").hide();
    $("#lblDescripcion").hide();
    $("#Descripcion").hide();
    $("#lblEmpieza").hide();
    $("#Empieza").hide();
    $("#lblTermina").hide();
    $("#Termina").hide();
    $("#lblFechaEvento").hide();
    $("#FechaEvento").hide();

    $("#Tipo").change(function () {
        var t = $(this).val();
        if (t == 1) {
            $("#lblTitulo").show();
            $("#Titulo").show();
            $("#lblDescripcion").show();
            $("#Descripcion").show();
            $("#lblEmpieza").hide();
            $("#Empieza").hide();
            $("#lblTermina").hide();
            $("#Termina").hide();
            $("#lblFechaEvento").show();
            $("#FechaEvento").show();
        } else {
            $("#lblTitulo").hide();
            $("#Titulo").hide();
            $("#lblDescripcion").hide();
            $("#Descripcion").hide();
            $("#lblEmpieza").hide();
            $("#Empieza").hide();
            $("#lblTermina").hide();
            $("#Termina").hide();
            $("#lblFechaEvento").hide();
            $("#FechaEvento").hide();
        }
    });
});