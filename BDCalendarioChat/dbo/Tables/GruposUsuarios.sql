﻿CREATE TABLE [dbo].[GruposUsuarios] (
    [grupo_id]   NVARCHAR (36) NOT NULL,
    [usuario_id] INT           NOT NULL,
    [creador]    BIT           NOT NULL,
    CONSTRAINT [PK_GruposUsuarios] PRIMARY KEY CLUSTERED ([grupo_id] ASC, [usuario_id] ASC),
    CONSTRAINT [FK_GruposUsuarios_grupo_id] FOREIGN KEY ([grupo_id]) REFERENCES [dbo].[Grupos] ([id]) ON DELETE CASCADE,
    CONSTRAINT [FK_GruposUsuarios_usuario_id] FOREIGN KEY ([usuario_id]) REFERENCES [dbo].[Usuarios] ([user_id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_FK_GruposUsuarios_usuario_id]
    ON [dbo].[GruposUsuarios]([usuario_id] ASC);

