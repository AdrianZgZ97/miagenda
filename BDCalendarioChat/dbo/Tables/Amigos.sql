﻿CREATE TABLE [dbo].[Amigos] (
    [usuario1_id] INT      NOT NULL,
    [usuario2_id] INT      NOT NULL,
    [fecha]       DATETIME NOT NULL,
    CONSTRAINT [PK_Amigos] PRIMARY KEY CLUSTERED ([usuario1_id] ASC, [usuario2_id] ASC)
);



