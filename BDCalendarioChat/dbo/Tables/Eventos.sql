﻿CREATE TABLE [dbo].[Eventos] (
    [evento_id]     INT            IDENTITY (1, 1) NOT NULL,
    [titulo]        NVARCHAR (20)  NOT NULL,
    [descripcion]   NVARCHAR (100) NULL,
    [empieza]       DATETIME       NULL,
    [termina]       DATETIME       NULL,
    [todoElDia]     BIT            NULL,
    [fechaEvento]   DATETIME       NULL,
    [tipo]          INT            NOT NULL,
    [estado]        NVARCHAR (20)  NOT NULL,
    [calendario_id] INT            NOT NULL,
    CONSTRAINT [PK_Eventos] PRIMARY KEY CLUSTERED ([evento_id] ASC),
    CONSTRAINT [FK_Eventos_calendario_id] FOREIGN KEY ([calendario_id]) REFERENCES [dbo].[Calendarios] ([calendario_id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Eventos_calendario_id]
    ON [dbo].[Eventos]([calendario_id] ASC);

