﻿CREATE TABLE [dbo].[Tareas] (
    [tarea_id] INT           IDENTITY (1, 1) NOT NULL,
    [titulo]   NVARCHAR (20) NOT NULL,
    [estado]   BIT           NOT NULL,
    [lista_id] INT           NOT NULL,
    CONSTRAINT [PK_Tareas] PRIMARY KEY CLUSTERED ([tarea_id] ASC),
    CONSTRAINT [FK_Tareas_lista_id] FOREIGN KEY ([lista_id]) REFERENCES [dbo].[ListasTareas] ([lista_id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Tareas_lista_id]
    ON [dbo].[Tareas]([lista_id] ASC);

