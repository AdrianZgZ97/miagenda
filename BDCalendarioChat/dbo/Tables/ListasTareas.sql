﻿CREATE TABLE [dbo].[ListasTareas] (
    [lista_id] INT           IDENTITY (1, 1) NOT NULL,
    [nombre]   NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_ListasTareas] PRIMARY KEY CLUSTERED ([lista_id] ASC)
);

