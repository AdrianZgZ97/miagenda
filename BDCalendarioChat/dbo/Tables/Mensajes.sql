﻿CREATE TABLE [dbo].[Mensajes] (
    [id]         INT            IDENTITY (1, 1) NOT NULL,
    [mensaje]    NVARCHAR (200) NOT NULL,
    [usuario_id] INT            NOT NULL,
    [chat_id]    INT            NOT NULL,
    CONSTRAINT [PK_Mensajes] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Mensajes_chat_id] FOREIGN KEY ([chat_id]) REFERENCES [dbo].[Chats] ([id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Mensajes_usuario_id] FOREIGN KEY ([usuario_id]) REFERENCES [dbo].[Usuarios] ([user_id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_FK_Mensajes_usuario_id]
    ON [dbo].[Mensajes]([usuario_id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Mensajes_chat_id]
    ON [dbo].[Mensajes]([chat_id] ASC);

