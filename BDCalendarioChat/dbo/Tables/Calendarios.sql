﻿CREATE TABLE [dbo].[Calendarios] (
    [calendario_id] INT           IDENTITY (1, 1) NOT NULL,
    [nombre]        NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_Calendarios] PRIMARY KEY CLUSTERED ([calendario_id] ASC)
);

