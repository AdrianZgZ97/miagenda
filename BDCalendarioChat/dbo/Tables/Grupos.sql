﻿CREATE TABLE [dbo].[Grupos] (
    [id]      NVARCHAR (36) NOT NULL,
    [nombre]  NVARCHAR (30) NOT NULL,
    [privado] BIT           NOT NULL,
    [chat_id] INT           NOT NULL,
    CONSTRAINT [PK_Grupos] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Grupos_chat_id] FOREIGN KEY ([chat_id]) REFERENCES [dbo].[Chats] ([id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_FK_Grupos_chat_id]
    ON [dbo].[Grupos]([chat_id] ASC);

