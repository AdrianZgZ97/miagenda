﻿CREATE TABLE [dbo].[ListasTareasUsuariosSet] (
    [lista_id] INT NOT NULL,
    [user_id]  INT NOT NULL,
    [creador]  BIT NOT NULL,
    CONSTRAINT [PK_ListasTareasUsuariosSet] PRIMARY KEY CLUSTERED ([lista_id] ASC, [user_id] ASC),
    CONSTRAINT [FK_lista_id] FOREIGN KEY ([lista_id]) REFERENCES [dbo].[ListasTareas] ([lista_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ListasTareasUsuariosSet_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[Usuarios] ([user_id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_ListasTareasUsuariosSet_user_id]
    ON [dbo].[ListasTareasUsuariosSet]([user_id] ASC);

