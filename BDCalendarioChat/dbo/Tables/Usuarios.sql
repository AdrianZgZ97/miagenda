﻿CREATE TABLE [dbo].[Usuarios] (
    [user_id]  INT           IDENTITY (1, 1) NOT NULL,
    [usuario]  NVARCHAR (20) NOT NULL,
    [password] NVARCHAR (30) NOT NULL,
    [email]    NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED ([user_id] ASC)
);

