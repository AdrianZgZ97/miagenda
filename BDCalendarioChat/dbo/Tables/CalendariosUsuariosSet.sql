﻿CREATE TABLE [dbo].[CalendariosUsuariosSet] (
    [calendario_id] INT NOT NULL,
    [user_id]       INT NOT NULL,
    [creador]       BIT NOT NULL,
    CONSTRAINT [PK_CalendariosUsuariosSet] PRIMARY KEY CLUSTERED ([calendario_id] ASC, [user_id] ASC),
    CONSTRAINT [FK_calendario_id] FOREIGN KEY ([calendario_id]) REFERENCES [dbo].[Calendarios] ([calendario_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_CalendariosUsuariosSet_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[Usuarios] ([user_id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CalendariosUsuariosSet_user_id]
    ON [dbo].[CalendariosUsuariosSet]([user_id] ASC);

